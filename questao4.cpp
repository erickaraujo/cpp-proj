#include <iostream>
#include <conio.h>
#include <iomanip> //usa a função "std::setw(2)", é equivalente ao "%2d" do printf. 

main(){
    int tamanho = 20;
    int elem[tamanho];
    int num;
    int valorIgual = 10;

    std::cout<<"Digite 20 elementos: ";
    for(int i = 0; i<tamanho; i++){
        std::cin>>num;
        elem[i] = num;
    }

    for(int i = 0; i<tamanho; i++){
        if(elem[i] == valorIgual){
            std::cout<<"Encontrado valor 10 na posicao: "<<i<<"\n";
        }
    }
}