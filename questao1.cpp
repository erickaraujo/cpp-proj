#include <iostream>
#include <conio.h>
#include <iomanip> //usa a função "std::setw(2)", é equivalente ao "%2d" do printf. 

main(){
    int tamanho = 5;
    int vetor[tamanho];
    int i; // %d
    float x; //número real = float %f

    vetor[1] = 2;
    vetor[2] = 4;
    vetor[3] = 1;
    vetor[4] = 2;
    vetor[5] = 5;

    x = vetor[1] + vetor[5]; std::cout << "Resultado de 'vetor[1] + vetor[5]' -> "  << x;
    x = vetor[2] - vetor[5]; std::cout << "\nResultado de 'vetor[2] - vetor[5]' -> "  << x;
    x = vetor[4] * vetor[1] - x; std::cout << "\nResultado de 'vetor[4] * vetor[1] - x' -> " << x;
    i = 3;
    x = vetor[i]; std::cout<< "\nResultado de 'x = vetor[i]' -> " << x;
    x = vetor[i] / vetor[vetor[1]]; std::cout << "\nResultado de 'vetor[i] / vetor[vetor[1]]' -> " << x;

}