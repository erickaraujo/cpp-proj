#include <iostream>
#include <conio.h>
#include <iomanip> //usa a função "std::setw(2)", é equivalente ao "%2d" do printf. 
#include <cstdlib>
#include <ctime>

int RandomInt(int min, int max){
    return (int)(rand() / (RAND_MAX / (double)(max - min))) + min;
}

main(){
    int tam = 100;
    int vetor[tam];
    int num = 0, 
        cont = 0, 
        media = 0, 
        total = 0,
        contIgual = 0,
        contMaior = 0;

    std::cout<<"Escrevendo 100 numeros... \n";

    // gerando 100 numeros aleatorios de 0 a 100
    srand(time(0));
    for (int i = 0; i<tam; i++){    
        num = RandomInt(1, 100);
        // std::cin>>num //se for para o usuário inputar os 100 numeros reais, descomenta essa linha e comenta a chamada ao RandomInt();
        std::cout<<"Número escolhido: "<<num<<"\n";
        vetor[i] = num;

        total += num;
        
        if(num == 30){
            cont++;
        }
    }

    media = total/tam;
    for(int i=0; i<tam; i++){
        if(vetor[i] == media){
            contIgual++;
        }
        if(vetor[i] > media){
            contMaior++;
        }
    }
    std::cout<<"\n";

    std::cout<<cont<<" números são iguais a 30 \n\n";
    std::cout<<"MEDIA: "<<media<<"\n";
    std::cout<<contIgual<<" números são iguais a media \n";
    std::cout<<contMaior<<" números são maiores que a media\n";
}