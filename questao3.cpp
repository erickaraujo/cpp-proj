#include <iostream>
#include <conio.h>
#include <iomanip> //usa a função "std::setw(2)", é equivalente ao "%2d" do printf. 
#include <vector>

main(){
    int tamanho = 10;
    int a, n;
    int alunos[tamanho];
    float media = 0;
    float notasTotal = 0;
    int alunosAprovados = 0;

    for (int b = 0; b<tamanho; b++){
        std::cout<<"Informe a nota do Aluno "<<std::setw(2)<<b<<":";
        std::cin>>n;
        
        alunos[b] = n;
        notasTotal += n;
        std::cout<<"Total de Notas calculadas:"<<notasTotal<<"\n\n";
    }

    media = notasTotal/10;

    std::cout<<"Media: "<<media;
    for(int b=0; b<tamanho; b++){
        if(alunos[b] >= media){
            std::cout<<"\nAluno "<<b<<" Aprovado!";
            alunosAprovados++;
        }
    }
    std::cout<<"\n"<<alunosAprovados<<" alunos aprovados!";
}