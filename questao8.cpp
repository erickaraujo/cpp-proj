#include <iostream>
#include <conio.h>
#include <iomanip> //usa a função "std::setw(2)", é equivalente ao "%2d" do printf. 


main(){
    int tam = 8;
    int vetor[tam];
    int input;
    int cont = 0;
    
    std::cout<<"Digite 8 valores:";
    for(int i=0; i<tam; i++){
        std::cin>>input;
        vetor[i] = input;
    }

    std::cout<<"\n\nImprimindo valores digitados: ";
    for(int i=0; i<tam; i++){
        std::cout<<vetor[i]<<" ";
    }

    std::cout<<"\n";
    for(int i=0; i<tam; i++){
        if(vetor[i] % 6 == 0){
            cont++;
            std::cout<<"\nO valor "<<vetor[i]<< " e multiplo de 6 !";
        }
    }
    std::cout<<"\n\nTotal de "<<cont<<" numeros multiplos de 6 !";
}