#include <vector>
#include <iostream>
#include <cstdlib>
#include <ctime>

int RandomInt(int min, int max){ //classe para gerar um numero randomico
    return (int)(rand() / (RAND_MAX / (double)(max - min))) + min; 
}


int main(){
    int tam = 30;
    int vetor[tam];
    int num = 0;

    for (int i=0; i<tam; i++){
        num = RandomInt(1, 100);
        vetor[i] = num;
        std::cout<<"valor escolhido: "<<num<<"\n";
    }

    std::cout<"\n\n ";
    std::cout<"Vetor Revertido: ";
    for(int i=tam-1; i >= 0; i--){
        std::cout<<vetor[i]<<" valor\n";
    }

    //ha outras formas de fazer isso, usando o std::vector<int> variavel(min, max); etc é bem mais simples, com a função reverse();
}