#include <iostream>
#include <conio.h>
#include <iomanip> //usa a função "std::setw(2)", é equivalente ao "%2d" do printf. 

//USA SEMPRE O 'COUT' E 'CIN' INVÉS DE PRINTF() E SCANF()....................
//Nao usei o "using namespace std" pra evitar qualquer problema com compatibilidade de eu referenciar algum arquivo aqui, nesse exemplo não faz muita diferença

main(){

    int size = 10;

    int A[size], B[size];
    int i;

    char TECLA;

    for (i = 0; i<size; i++){
        std::cout<<"informe um valor para o elemento numero"<<std::setw(2)<<":";
        std::cin >> A[i];
    }

    for (i = 0; i<size; i++){
        if(i % 2 == 0)
            B[i] = A[i] * 5;
        else
            B[i] = A[i] + 5;
    }

    for (i = 0; i < size; i++){
        //std::cout << "\n A[%2d] = %2d, B[%2d] = %2d" << i << A[i] << B[i]; ////o %2d não é compativel com o 'cout'
        std::cout << "\n A["<<std::setw(2)<< A[i] <<"] = "<<std::setw(2)<< B[i] <<" = "<<std::setw(2)<<  i;
    }
    std::cout << "\n\n Tecle para encerrar...";
    TECLA = getche();
}