#include <iostream>
#include <conio.h>
#include <iomanip> //usa a função "std::setw(2)", é equivalente ao "%2d" do printf. 


main(){
    int tam = 7;
    int media = 0;
    int res;
    int temp[7] = {19, 23, 21, 25, 22, 20, 24};

    std::cout<<"Media acumulada: ";
    for(int i = 0; i<tam; i++){
        media += temp[i];
        std::cout<<media<<" ";
    }

    res = media/tam;

    std::cout<<"\nMedia de Temperatura: "<<res;
}