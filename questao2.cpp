#include <iostream>
#include <conio.h>
#include <iomanip> //usa a função "std::setw(2)", é equivalente ao "%2d" do printf. 

main(){
    int tamanho = 10;

    int A[tamanho], B[tamanho];
    int i;

    char TECLA;

    for (i = 0; i<tamanho; i++){
        std::cout<<"informe um valor para o elemento numero"<<std::setw(2)<<i<<":";
        std::cin >> A[i];
    }

    for (i = 0; i<tamanho; i++){
        if(i % 2 == 0)
            B[i] = A[i] * 5;
        else
            B[i] = A[i] + 5;
    }

    for (i = 0; i < tamanho; i++){
        std::cout << "\n A["<<std::setw(2)<< A[i] <<"] -->  "
                  <<"Input = " <<std::setw(1)<<  i
                  <<" --> B["<<std::setw(2)<< B[i]<<"]";
    }
    std::cout << "\n\n Tecle para encerrar...";
    TECLA = getche();
}